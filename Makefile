all: index.html

index.html: usage_example.asciidoc
	    asciidoctor $< -o index.html
