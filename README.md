# README #

This repository contains source code and usage examples to detect large 
chromosomal inversions with chromosome conformation capture sequencing 
(Hi-C) data and to simulate Hi-C contact matrices.
